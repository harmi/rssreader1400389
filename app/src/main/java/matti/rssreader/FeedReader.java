package matti.rssreader;

import android.util.Log;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by Matti on 22.9.2015.
 */
public class FeedReader implements Runnable {
    private static final String TAG = "RSSReader";
    private URL yleFeedURL;
    private URL hsFeedURL;
    private XMLParser xmlParser;
    private InputStream xmlInputStream;
    private String userRssUrl;
    private URL userFeed;
    private boolean failCheck = false;
    private boolean urlChecked = false;

    public FeedReader(XMLParser xmlParser) {
        this.xmlParser = xmlParser;
        Log.e(TAG, "New FeedReader created");
    }

    public FeedReader(String userRssUrl) throws XmlPullParserException {
        this.userRssUrl = userRssUrl;
        Log.d(TAG, "User input  " + this.userRssUrl);
        this.xmlParser = new XMLParser();
    }

    @Override
    public void run() {
        openUserFeedStream();
        // sending the object InputStream to XML parser.
        if (!this.failCheck) {

            try {
                this.xmlParser.parseXMLLines(this.xmlInputStream);
            } catch (XmlPullParserException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        else {
            Log.d(TAG,"The url was malformed.");
        }
    }

    private void openUserFeedStream() {
        this.failCheck = false;
        this.urlChecked = false;
        try {
            this.userFeed = new URL(this.userRssUrl);
            if (!this.failCheck) {
                Log.d(TAG, "User feed url created.");
                Log.e(TAG, "Opening new input stream from RSS user feed " + this.userFeed);
                this.xmlInputStream = this.userFeed.openStream();
                this.urlChecked = true;
                Log.e(TAG, "Print. " + this.xmlInputStream);
            } else {
                Log.e(TAG, "FailCheck wasn't passed.");
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
            Log.e(TAG, "Something was wrong with the URL.");
            this.failCheck = true;
            this.urlChecked = true;
            Log.e(TAG, "This " + this.failCheck);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}