package matti.rssreader;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Matti on 8.10.2015.
 */
public class FavoriteFeedsAdapter extends ArrayAdapter<FeedItem> {
    private static final String TAG = "RSSReader";
    private ArrayList<FeedItem> favoriteFeeds;
    private Context context;
    private TextView cellText_header;
    private TextView cellText_body;

    public FavoriteFeedsAdapter(Context context, int resource, ArrayList<FeedItem> favoriteFeeds) {
        super(context, resource, favoriteFeeds);
        this.favoriteFeeds = favoriteFeeds;
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = li.inflate(R.layout.cell_layout, null);

        this.cellText_header = (TextView) convertView.findViewById(R.id.cellText_header);
        this.cellText_body = (TextView) convertView.findViewById(R.id.cellText_body);

        this.cellText_header.setText(this.favoriteFeeds.get(position).getFeedItemTitle());
        this.cellText_body.setText(this.favoriteFeeds.get(position).getPubDate());
        return convertView;
    }
}


