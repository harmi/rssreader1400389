package matti.rssreader;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.support.v7.widget.Toolbar;

import org.xmlpull.v1.XmlPullParserException;

public class FeedView extends AppCompatActivity implements NewFeed, NavigationView.OnNavigationItemSelectedListener {
    private static final String TAG = "RSSReader";
    public final static String EXTRA_MESSAGE = "RSSReader";
    private ListView listViewFeedView;
    private FeedViewListAdapter feedViewListAdapter;
    private String userGivenURL;
    private User user;
    private Feed feed;
    private FeedReader feedReader;
    private Thread feedReaderThread;
    private Intent feedItemsIntent;
    private Intent feedViewIntent;
    private Intent favoritesIntent;
    private Intent settingsIntent;
    private Intent newFeedItemsIntent;
    private String[] menuItems = {"Delete feed", ""};
    private int listItemClicked;
    private int menuItemClicked;
    private View coordinatorLayoutView;
    private Toolbar toolbar;
    private CustomDrawerLayout drawerLayout;

    // TODO:
    // link: http://hmkcode.com/material-design-app-android-design-support-library-appcompat/
    // link2: https://guides.codepath.com/android
    // http://stackoverflow.com/questions/7883753/switching-between-activities-within-a-tab-bar-layout
    // http://www.android4devs.com/2015/01/how-to-make-material-design-sliding-tabs.html
    // add some sort of refresh mechanism
    // add saving
    // add help/info screen
    // add pop up menu for favorites


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feed_view);
        this.coordinatorLayoutView = findViewById(R.id.main_content_feedview);
        this.listViewFeedView = (ListView) findViewById(R.id.listViewFeedView);
        user = User.getUser();
        this.feedItemsIntent = new Intent(this, MainActivity.class);
        setupNavigationView();
        setupToolbar();
        registerToFeed();
        makeListView();
        // register ListView to ContextMenu so that pop up can be created
        registerForContextMenu(this.listViewFeedView);
        this.listViewFeedView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                feed = feedViewListAdapter.getItem(position);
                Log.d(TAG, "Position clicked  " + position);
                Log.d(TAG, "Something gathered by onItemClick " + feed.toString());
                feedItemsIntent.putExtra("PERKELE", position);
                startActivity(feedItemsIntent);
            }
        });
    }

    // called when the FAB is clicked
    // builds an alert with EditText and two buttons
    // the text user inputs into the field goes to addFeedFromURL();
    public void addFeedButtonClicked(View view) {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle("Add new feed");
        alert.setMessage("Write feed URL");
        final EditText rssInput = new EditText(this);
        alert.setView(rssInput);
        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                userGivenURL = rssInput.getText().toString();
                Log.e(TAG, "User wrote " + userGivenURL);
                //adding the feed
                try {
                    addFeedFromURL();
                } catch (XmlPullParserException e) {
                    e.printStackTrace();
                }
            }
        });

        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        alert.show();
    }

    // creates the Feed from the URL user input into the FAB
    public void addFeedFromURL() throws XmlPullParserException {
        this.feedReader = new FeedReader(this.userGivenURL);
        this.feedReaderThread = new Thread(this.feedReader);
        this.feedReaderThread.start();
        makeAddedFeedSnackbar();
    }

    public void makeAddedFeedSnackbar() {
        Snackbar.make(this.coordinatorLayoutView, "Added a new feed.", Snackbar.LENGTH_SHORT)
                .show();
    }

    // to create a pop up menu when long pressing a ListItem
    @Override
    public void onCreateContextMenu(ContextMenu menu, View view, ContextMenu.ContextMenuInfo menuInfo) {
        // check if the item clicked can be found from the ListViewFeedView
        if (view.getId() == R.id.listViewFeedView) {
            AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
            Log.e(TAG, "User long pressed a ListView item in position" + info.position);
            // setting the position that the user clicked, so that it can be used later
            this.listItemClicked = info.position;
            menu.setHeaderTitle("Modify feed");
            Log.e(TAG, "Menu items in list " + this.menuItems.length);
            for (int i = 0; i < this.menuItems.length; i++) {
                // NONE = no grouping, i & i = order and ID of the items are the same
                // this.menuItems = to add the String from the array
                menu.add(menu.NONE, i, i, this.menuItems[i]);
            }
        }
    }

    // to get info on what pop up menu item user clicked
    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        int menuItemIndex = item.getItemId();
        String menuItemName = this.menuItems[menuItemIndex];
        this.menuItemClicked = item.getItemId();
        Log.e(TAG, "User clicked " + this.menuItemClicked);
        // if user clicked Delete
        if (this.menuItemClicked == 0) {
            removeFeed();
        } else {
            Log.e(TAG, "User clicked something else");
        }
        // the method has to have a boolean return value
        return true;
    }

    public void removeFeed() {
        user.removeFeed(listItemClicked);
        makeRemovedFeedSnackBar();
    }

    // creates a snackbar informing that a feed has been removed
    public void makeRemovedFeedSnackBar() {
        Snackbar.make(this.coordinatorLayoutView, "Removed a feed.", Snackbar.LENGTH_LONG)
                .setAction("Undo", reverseRemovalListener)
                .setActionTextColor(Color.rgb(250, 0, 0))
                .show();
    }

    // onClickListener for the reverse removal button in snackbar
    View.OnClickListener reverseRemovalListener = new View.OnClickListener() {
        public void onClick(View view) {
            user.reverseRemoval(listItemClicked);
        }
    };

    private void makeListView() {
        this.feedViewListAdapter = new FeedViewListAdapter(this, R.layout.activity_main, user.getListOfFeeds());
        Log.i(TAG, "Created a new FeedViewListAdapter");
        this.listViewFeedView.setAdapter(this.feedViewListAdapter);
    }

    @Override
    public void updateUI() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                feedViewListAdapter.notifyDataSetChanged();
            }
        });
    }

    public void registerToFeed() {
        user.register(this);
    }

    // setting up the toolbar, overrides the default ActioBar
    private void setupToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeAsUpIndicator(R.drawable.ic_menu_black_24dp);
        actionBar.setDisplayHomeAsUpEnabled(true);
    }

    // setting up the navigation view (the menu from the side)
    private void setupNavigationView() {
        this.drawerLayout = (CustomDrawerLayout) findViewById(R.id.drawerLayout);
        NavigationView navigationView = (NavigationView) findViewById(R.id.navigationView);
        navigationView.setNavigationItemSelectedListener(this);
    }

    // listener for the NavigationView items
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // user clicks the home button -> opens the menu
            case R.id.list_of_feeds:
                Log.e(TAG, "Accessing feedview");
                this.feedViewIntent = new Intent(this, FeedView.class);
                startActivity(this.feedViewIntent);
                this.drawerLayout.closeDrawer(GravityCompat.START);
                return true;
            case R.id.favorites:
                Log.e(TAG, "Accessing favorites");
                this.favoritesIntent = new Intent(this, FavoriteActivity.class);
                startActivity(this.favoritesIntent);
                this.drawerLayout.closeDrawer(GravityCompat.START);
                return true;
            case R.id.new_feed_items:
                Log.e(TAG, "Accessing NEW");
                this.newFeedItemsIntent = new Intent(this, NewFeedItemsActivity.class);
                startActivity(this.newFeedItemsIntent);
                this.drawerLayout.closeDrawer(GravityCompat.START);
                return true;
         /*   case R.id.settings:
                Log.e(TAG, "Accessing settings");
                //this.settingsIntent = new Intent(this, settingsActivity.class);
                //startActivity(this.settingsActivity);
                this.drawerLayout.closeDrawer(GravityCompat.START);
                return true; */
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    //  opens the NavigationView inside the DrawerLayout
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.e(TAG, "ID clicked " + item.getTitle());
        switch (item.getItemId()) {
            // user clicks the home button -> opens the menu
            case android.R.id.home:
                Log.e(TAG, "Home button clicked");
                this.drawerLayout.openDrawer(GravityCompat.START);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_feed_view, menu);
        return true;
    }

    // the failed saving system
/*    @Override
    public void onPause() {
        super.onPause();
        Log.e(TAG, "Started saving the items");
        try {
            FileOutputStream out = openFileOutput("rssreader", Context.MODE_PRIVATE);
            ObjectOutputStream outputStream = new ObjectOutputStream(out);
            outputStream.writeObject(user.getListOfFeeds());
            out.close();
            outputStream.close();
        } catch (FileNotFoundException e) {
            Log.e(TAG, "Couldn't open file");
            e.printStackTrace();
        } catch (IOException e) {
            Log.e(TAG, "Couldn't write into file");
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        FileInputStream in = null;
        Log.e(TAG, "Starting to load the items.");
        try {
            in = openFileInput("rssreader");
            ObjectInputStream obin = new ObjectInputStream(in);
            user.replaceFeeds((ArrayList<Feed>) obin.readObject());

            this.feedViewListAdapter = new FeedViewListAdapter(this, R.layout.activity_feed_view, user.getListOfFeeds());
            this.listViewFeedView.setAdapter(this.feedViewListAdapter);
        } catch (FileNotFoundException e) {
            Log.e(TAG, "NO save file found.");
        } catch (StreamCorruptedException e) {
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    } */
}