package matti.rssreader;

import android.content.Intent;
import android.net.Uri;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;

public class NewFeedItemsActivity extends AppCompatActivity implements NewFeed, NavigationView.OnNavigationItemSelectedListener {
    public static final String TAG = "RSSReader";
    private User user;
    private CustomDrawerLayout drawerLayout;
    private NavigationView navigationView;
    private Toolbar toolbar;
    private ListView listView;
    private LinearLayout main_content_favoritesView;
    private NewFeedItemsAdapter newFeedItemsAdapter;
    private Intent browserIntent;
    private FeedItem feedItem;
    private Intent feedViewIntent;
    private Intent favoritesIntent;
    private Intent settingsIntent;
    private Intent newFeedItemsIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorite);
        user = User.getUser();
        user.generateNewFeedItems();
        setupNavigationView();
        setupToolbar();
        this.listView = (ListView) findViewById(R.id.listView);
        this.main_content_favoritesView = (LinearLayout) findViewById(R.id.main_content_favoritesView);
        registerToFeed();
        // registering the listview in order to enable pop up menu
        registerForContextMenu(this.listView);
        makeListView();

        // makes feedItems clickable -> opens it in the browser
        this.listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                feedItem = newFeedItemsAdapter.getItem(position);
                // setting the feeditem as read
                newFeedItemsAdapter.getItem(position).setReadStatusTrue();
                browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(feedItem.getFeedItemLink()));
                startActivity(browserIntent);

            }
        });
    }

    private void makeListView() {
        this.newFeedItemsAdapter = new NewFeedItemsAdapter(this, R.layout.activity_favorite, user.getNewFeedItems());
        this.listView.setAdapter(this.newFeedItemsAdapter);
    }

    @Override
    public void updateUI() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                user.printAllFeedItems();
                newFeedItemsAdapter.notifyDataSetChanged();
            }
        });
    }

    public void registerToFeed() {
        user.register(this);
    }

    // setting up the toolbar
    private void setupToolbar() {
        this.toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(this.toolbar);

        final ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeAsUpIndicator(R.drawable.ic_menu_black_24dp);
        actionBar.setDisplayHomeAsUpEnabled(true);
    }

    // finds drawerLayout & sets this activity to NavigationItemSelectedListener (onClick)
    private void setupNavigationView() {
        this.drawerLayout = (CustomDrawerLayout) findViewById(R.id.drawerLayout);
        this.navigationView = (NavigationView) findViewById(R.id.navigationView);
        this.navigationView.setNavigationItemSelectedListener(this);
    }

    //  opens the NavigationView inside the DrawerLayout
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.e(TAG, "ID clicked " + item.getItemId());
        switch (item.getItemId()) {
            // user clicks the home button -> opens the menu
            case android.R.id.home:
                Log.e(TAG, "Home button clicked");
                this.drawerLayout.openDrawer(GravityCompat.START);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    // listener for the NavigationView items
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        Log.e(TAG, "ID clicked " + item.getItemId());
        switch (item.getItemId()) {
            case R.id.list_of_feeds:
                Log.e(TAG, "Accessing feedview");
                this.feedViewIntent = new Intent(this, FeedView.class);
                startActivity(this.feedViewIntent);
                this.drawerLayout.closeDrawer(GravityCompat.START);
                return true;
            case R.id.favorites:
                Log.e(TAG, "Accessing favorites");
                this.favoritesIntent = new Intent(this, FavoriteActivity.class);
                startActivity(this.favoritesIntent);
                this.drawerLayout.closeDrawer(GravityCompat.START);
                return true;
            case R.id.new_feed_items:
                Log.e(TAG, "Accessing NEW");
                // this.newFeedItemsIntent = new Intent(this, newItemsActivity.class);
                // startActivity(this.newItemsActivity);
                this.drawerLayout.closeDrawer(GravityCompat.START);
                return true;
         /*   case R.id.settings:
                Log.e(TAG, "Accessing settings");
                //this.settingsIntent = new Intent(this, settingsActivity.class);
                //startActivity(this.settingsActivity);
                this.drawerLayout.closeDrawer(GravityCompat.START);
                return true; */
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_favorite, menu);
        return true;
    }

 /*   @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    } */
}
