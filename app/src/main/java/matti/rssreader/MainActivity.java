package matti.rssreader;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;

import org.xmlpull.v1.XmlPullParserException;


public class MainActivity extends AppCompatActivity implements NewFeed, NavigationView.OnNavigationItemSelectedListener {
    private static final String TAG = "RSSReader";
    private ListView listView;
    private FeedReader feedReader;
    private XMLParser xmlParser;
    private FeedListAdapter feedListAdapter;
    private User user;
    private Intent intent;
    private FeedItem feedItem;
    private Intent browserIntent;
    private Intent shareIntent;
    private Intent feedViewIntent;
    private Intent favoritesIntent;
    private Intent settingsIntent;
    private Intent newFeedItemsIntent;
    private int userSelection = 0;
    private String[] menuItems = {"Favorite", "Delete", "Share"};
    private String[] menuItemsUnlike = {"Unfavorite", "Delete", "Share"};
    private int listItemClicked;
    private int menuItemClicked;
    private LinearLayout main_content_feedItemView;
    private Toolbar toolbar;
    private CustomDrawerLayout drawerLayout;
    private NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        user = User.getUser();
        setupNavigationView();
        setupToolbar();
        // finding the TexView, ScrollView and ListView
        this.listView = (ListView) findViewById(R.id.listView);
        this.main_content_feedItemView = (LinearLayout) findViewById(R.id.main_content_feedItemView);
        try {
            this.xmlParser = new XMLParser();
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        }
        this.feedReader = new FeedReader(this.xmlParser);
        registerToFeed();
        // registering ListView to enable pop up menu
        registerForContextMenu(this.listView);
        this.intent = getIntent();
        this.userSelection = this.intent.getExtras().getInt("PERKELE");
        Log.e(TAG, "Intent received, intent " + this.userSelection);
      //  user.updatedAllFeedItemsList();
        makeListView();
        // making feeditems clickable -> opens the browser
        this.listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                feedItem = feedListAdapter.getItem(position);
                // setting the feeditem as read
                feedListAdapter.getItem(position).setReadStatusTrue();
                browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(feedItem.getFeedItemLink()));
                startActivity(browserIntent);

            }
        });
    }


    private void makeListView() {
            this.feedListAdapter = new FeedListAdapter(this, R.layout.activity_main, user.getFeed(this.userSelection).getFeedItemList());
            Log.i(TAG, "Created a new feedListAdapter, FeedItemList size is " + user.getAllFeedItems().size());
            this.listView.setAdapter(this.feedListAdapter);
    }

    public void updateUI() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                user.printAllFeedItems();
                feedListAdapter.notifyDataSetChanged();
            }
        });
    }

    public void registerToFeed() {
        user.register(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    // creates a pop up menu when long pressing a ListItem
    @Override
    public void onCreateContextMenu(ContextMenu menu, View view, ContextMenu.ContextMenuInfo menuInfo) {
        if (view.getId() == R.id.listView) {
            AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
            Log.e(TAG, "In Main A0ctivity user long pressed item in pos " + info.position);
            this.listItemClicked = info.position;
            Log.e(TAG, "Feed item actually clicked " + user.getFeed(this.userSelection).getFeedItemList().get(this.listItemClicked).toString());
            menu.setHeaderTitle("Feed item");
            if (!user.getFeed(this.userSelection).getFeedItemList().get(this.listItemClicked).getFavouriteStatus()) {
                for (int i = 0; i < this.menuItems.length; i++) {
                    menu.add(menu.NONE, i, i, this.menuItems[i]);
                }
            } else {
                for (int i = 0; i < this.menuItemsUnlike.length; i++) {
                    menu.add(menu.NONE, i, i, this.menuItemsUnlike[i]);
                }
            }
        }
    }

    // get info on what pop up menu item user clicked
    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        this.menuItemClicked = item.getItemId();

        // favorite item
        if (this.menuItemClicked == 0 && !user.getFeed(this.userSelection).getFeedItemList().get(this.listItemClicked).getFavouriteStatus()) {
            Log.e(TAG, "User chose to favorite the feeditem");
            user.getFeed(this.userSelection).getFeedItemList().get(this.listItemClicked).setFavorite();
            // unfavorite item
        } else if (this.menuItemClicked == 0 && user.getFeed(this.userSelection).getFeedItemList().get(this.listItemClicked).getFavouriteStatus()) {
            Log.e(TAG, "User chose to unfavorite the article");
            user.getFeed(this.userSelection).getFeedItemList().get(this.listItemClicked).unfavorite();
            // delete item
        } else if (this.menuItemClicked == 1) {
            Log.e(TAG, "User chose to delete the item");
            user.getFeed(this.userSelection).removeFromFeedItemList(this.listItemClicked);
            makeRemovedItemSnackbar();
            // share item
        } else if (this.menuItemClicked == 2) {
            shareLink();
        }
        return true;
    }

    public void makeRemovedItemSnackbar() {
        Snackbar.make(this.main_content_feedItemView, "Removed a feed.", Snackbar.LENGTH_LONG)
                .setAction("Undo", reverseRemovalListener)
                .setActionTextColor(Color.rgb(250, 0, 0))
                .show();
    }

    View.OnClickListener reverseRemovalListener = new View.OnClickListener() {
        public void onClick(View view) {
            user.getFeed(userSelection).reverseItemRemoval(listItemClicked);
        }
    };

    // sharing a link
    public void shareLink() {
        this.shareIntent = new Intent(android.content.Intent.ACTION_SEND);
        this.shareIntent.setType("text/plain");
        this.shareIntent.putExtra(Intent.EXTRA_SUBJECT, user.getFeed(this.userSelection).getFeedItemList().get(this.listItemClicked).getFeedItemTitle());
        this.shareIntent.putExtra(Intent.EXTRA_TEXT, "Check out this link " + user.getFeed(this.userSelection).getFeedItemList().get(this.listItemClicked).getFeedItemLink());
        startActivity(Intent.createChooser(this.shareIntent, "SHARE THIS LINK"));
    }

    private void setupToolbar() {
        this.toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeAsUpIndicator(R.drawable.ic_menu_black_24dp);
        actionBar.setDisplayHomeAsUpEnabled(true);
    }

    private void setupNavigationView() {
        this.drawerLayout = (CustomDrawerLayout) findViewById(R.id.drawerLayout);
        this.navigationView = (NavigationView) findViewById(R.id.navigationView);
        this.navigationView.setNavigationItemSelectedListener(this);
    }

    //  opens the NavigationView inside the DrawerLayout
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.e(TAG, "ID clicked " + item.getItemId());
        switch (item.getItemId()) {
            // user clicks the home button -> opens the menu
            case android.R.id.home:
                Log.e(TAG, "Home button clicked");
                // overridePendingTransition(R.anim., R.anim.out_to_right);
                this.drawerLayout.openDrawer(GravityCompat.START);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    // listener for the NavigationView items
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        Log.e(TAG, "ID clicked " + item.getItemId());
        switch (item.getItemId()) {
            case R.id.list_of_feeds:
                Log.e(TAG, "Accessing feedview");
                this.feedViewIntent = new Intent(this, FeedView.class);
                startActivity(this.feedViewIntent);
                this.drawerLayout.closeDrawer(GravityCompat.START);
                return true;
            case R.id.favorites:
                Log.e(TAG, "Accessing favorites");
                this.favoritesIntent = new Intent(this, FavoriteActivity.class);
                startActivity(this.favoritesIntent);
                this.drawerLayout.closeDrawer(GravityCompat.START);
                return true;
            case R.id.new_feed_items:
                Log.e(TAG, "Accessing NEW");
                // this.newFeedItemsIntent = new Intent(this, newItemsActivity.class);
                // startActivity(this.newItemsActivity);
                this.drawerLayout.closeDrawer(GravityCompat.START);
                return true;
       /*     case R.id.settings:
                Log.e(TAG, "Accessing settings");
                //this.settingsIntent = new Intent(this, settingsActivity.class);
                //startActivity(this.settingsActivity);
                this.drawerLayout.closeDrawer(GravityCompat.START);
                return true; */
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    //obsolete methods

/*    // this runs when the refresh button is clicked
    public void refreshXML(View view) {
        Log.e(TAG, "Button clicked");
        Thread feedReaderThread = new Thread(feedReader);
        Log.e(TAG, "Staring a new thread for FeedReader.");
        feedReaderThread.start();
        feedListAdapter.notifyDataSetChanged();
    }

    public void refreshView(View view) {
        Log.e(TAG, "Refresh view button clicked.");
        // updates the feeditemlist to have all the feeds
        user.updatedAllFeedItemsList();
        // prints out the size of the feed item list
        user.printAllFeedItems();
        // updates the listView
        updateUI();
    } */


/*    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    } */
}
