package matti.rssreader;

import android.util.Log;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Matti on 25.9.2015.
 */
public class Feed implements Serializable {
    private static final String TAG = "RSSReader";
    transient private ArrayList<FeedItem> feedItemList;
    transient private ArrayList<FeedItem> removedFeedItemsList = new ArrayList<>();
    private String feedTitle = "";
    private int unreadArticlesAmount = 0;
    private int favouriteArticles = 0;
    private FeedItem removedFeedItem;
    transient User user;

    public Feed() {
        Log.e(TAG, "New feed created");
        this.feedItemList = new ArrayList<FeedItem>();
        Log.e(TAG, "ArrayList " + this.feedItemList + " created to hold FeedItems.");
        user = User.getUser();
    }

    public Feed(String feedTitle) {
        this.feedTitle = feedTitle;
        Log.d(TAG, "Created a new feed with title " + this.feedTitle);
    }

    public void addFeedItemsToList(FeedItem feedItem) {
        this.feedItemList.add(feedItem);
        Log.e(TAG, "New feed item added to list " + feedItem.toString());
        Log.e(TAG, "Calling updated observers from Feed class.");
        //  user.updateObservers();

    }

    public void printFeedItemList() {
        Log.e(TAG, "Starting to go through feed items list");
        Log.e(TAG, "The feed is " + this.feedTitle);
        Log.e(TAG, "The feedItem list size is " + this.feedItemList.size());
        for (FeedItem f : this.feedItemList) {
            f.printThis();
        }
    }

    public int unreadArticleAmount() {
        this.unreadArticlesAmount = 0;
        for (FeedItem feedItem : this.feedItemList) {
            if (!feedItem.getReadStatus()) {
                this.unreadArticlesAmount++;
            }
        }
        return this.unreadArticlesAmount;
    }

    public int favouriteArticleAmount() {
        this.favouriteArticles = 0;
        for (FeedItem feedItem : this.feedItemList) {
            if (feedItem.getFavouriteStatus()) {
                this.favouriteArticles++;
            }
        }
        return this.favouriteArticles;
    }

    // removes a feeditem and adds it to removedFeedItemsList
    public void removeFromFeedItemList(int position) {
        Log.e(TAG, "Feed item " + this.feedItemList.get(position).toString() + " is going to be removed.");
        this.removedFeedItemsList.add(this.feedItemList.get(position));
        this.feedItemList.remove(position);
        user.updateObservers();
    }

    public void reverseItemRemoval(int originalPosition) {
        Log.e(TAG, "Reversing the removal");
        // getting the last of the incas
        this.removedFeedItem = this.removedFeedItemsList.get(this.removedFeedItemsList.size() - 1);
        this.feedItemList.add(originalPosition, this.removedFeedItem);
        user.updateObservers();
    }

    public ArrayList<FeedItem> getFeedItemList() {
        return this.feedItemList;
    }

    public void setFeedTitle(String string) {
        this.feedTitle = string;
    }

    public String toString() {
        return this.feedTitle;
    }

    public String getFeedTitle() {
        return this.feedTitle;
    }

}
