package matti.rssreader;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;

/**
 * Created by Matti on 28.9.2015.
 */
public class FeedListAdapter extends ArrayAdapter<FeedItem> {
    private static final String TAG = "RSSReader";
    private ArrayList<FeedItem> feedItemList;
    private Context context;
    TextView cellText_header;
    TextView cellText_body;

    public FeedListAdapter(Context context, int resource, ArrayList<FeedItem> feedItemList) {
        super(context, resource, feedItemList);
        this.feedItemList = feedItemList;
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater li = (LayoutInflater) context.getSystemService
                (Context.LAYOUT_INFLATER_SERVICE);
        convertView = li.inflate(R.layout.cell_layout, null);

        this.cellText_header = (TextView) convertView.findViewById(R.id.cellText_header);
        this.cellText_body = (TextView) convertView.findViewById(R.id.cellText_body);

        if (position < this.feedItemList.size()) {
            if (this.feedItemList.get(position).getFavouriteStatus()) {
                this.cellText_header.setText("" + this.feedItemList.get(position).getFeedItemTitle());
                this.cellText_header.setTextColor(Color.rgb(250, 180, 100));
                this.cellText_body.setText("" + this.feedItemList.get(position).getPubDate());
                return convertView;
            } else if (this.feedItemList.get(position).getFavouriteStatus() && this.feedItemList.get(position).getReadStatus()) {
                this.cellText_header.setText("" + this.feedItemList.get(position).getFeedItemTitle());
                this.cellText_header.setTextColor(Color.rgb(250, 180, 100));
                this.cellText_body.setText("" + this.feedItemList.get(position).getPubDate());
                return convertView;
            } else if (!this.feedItemList.get(position).getReadStatus()) {
                this.cellText_header.setText("" + this.feedItemList.get(position).getFeedItemTitle());
                this.cellText_body.setText("" + this.feedItemList.get(position).getPubDate());
                return convertView;
            } else {
                this.cellText_header.setText("" + this.feedItemList.get(position).getFeedItemTitle());
                this.cellText_header.setTextColor(Color.LTGRAY);
                this.cellText_body.setText("" + this.feedItemList.get(position).getPubDate());
                return convertView;
            }
        } else {
            Log.i(TAG, "Something went wrong in FeedListAdapter.");
            this.cellText_header.setText("Error happened");
            return convertView;
        }

    }
}
