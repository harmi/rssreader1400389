package matti.rssreader;

import android.content.Context;
import android.graphics.Typeface;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Matti on 3.10.2015.
 */
public class FeedViewListAdapter extends ArrayAdapter<Feed> {
    private static final String TAG = "RSSReader";
    private ArrayList<Feed> listOfFeeds;
    private Context context;
    private TextView cellCircle;
    private TextView cellText_header;
    private TextView cellText_feed_info;


    public FeedViewListAdapter(Context context, int resource, ArrayList<Feed> listOfFeeds) {
        super(context, resource, listOfFeeds);
        this.listOfFeeds = listOfFeeds;
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup partent) {
        LayoutInflater li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = li.inflate(R.layout.cell_layout_feed, null);

        this.cellCircle = (TextView) convertView.findViewById(R.id.cellTextCircle);
        this.cellText_header = (TextView) convertView.findViewById(R.id.cellText_header);
        this.cellText_feed_info = (TextView) convertView.findViewById(R.id.cellText_feed_info);

        if (position < this.listOfFeeds.size()) {
            this.cellCircle.setText("" + this.listOfFeeds.get(position).unreadArticleAmount());
            this.cellText_header.setText("" + this.listOfFeeds.get(position).getFeedTitle());
            this.cellText_feed_info.setText("Articles " + this.listOfFeeds.get(position).getFeedItemList().size());
            this.cellText_feed_info.append(" Unread " + this.listOfFeeds.get(position).unreadArticleAmount());
            this.cellText_feed_info.append(" Favorite " + this.listOfFeeds.get(position).favouriteArticleAmount());

            return convertView;
        } else {
            Log.i(TAG, "Something went wrong in FeedViewListAdapter");
            return convertView;
        }

    }


}
