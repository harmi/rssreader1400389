package matti.rssreader;

import android.util.Log;

import java.util.ArrayList;

/**
 * Created by Matti on 25.9.2015.
 */
public class User {
    private static final String TAG = "RSSReader";
    private static User user = new User();
    private ArrayList<Feed> listOfFeeds;
    private ArrayList<Feed> removedFeeds = new ArrayList<>();
    private ArrayList<NewFeed> listOfObservers = new ArrayList();
    private ArrayList<FeedItem> allFeedItems = new ArrayList();
    private ArrayList<FeedItem> favoriteFeedItems = new ArrayList<>();
    private ArrayList<FeedItem> newFeedItems = new ArrayList<>();
    private Feed removedFeed;

    private User() {
        this.listOfFeeds = new ArrayList<>();
        Log.e(TAG, "New User object created.");
    }

    public void addToUserFeedList(Feed feed) {
        Log.e(TAG, "Adding a new Feed to the user Feed list.");
        this.listOfFeeds.add(feed);
        Log.e(TAG, "Added feed " + feed.toString() + " to user feedlist.");
        updateObservers();
    }

    public String toString() {
        return "User object has a list if size " + this.listOfFeeds.size();
    }

    public static User getUser() {
        return user;
    }

    public ArrayList<Feed> getListOfFeeds() {
        return listOfFeeds;
    }

    public Feed getFeed(int selection) {
        return listOfFeeds.get(selection);
    }

    public void updatedAllFeedItemsList() {
        Log.e(TAG, "Not used anymore, but might be used in the future");
    }

    // loops through all Feeds in listOfFeeds, then all feedItems in those Feed lists
    public ArrayList<FeedItem> getAllFeedItems() {
        return this.allFeedItems;
    }

    public void printAllFeedItems() {
        Log.e(TAG, "All feed items size  " + this.allFeedItems.size());
    }

    // removes a feed from the listOfFeeds and adds it to removedFeeds
    public void removeFeed(int position) {
        Log.e(TAG, "Removing feed " + this.listOfFeeds.get(position).toString() + " at position " + position);
        this.removedFeeds.add(this.listOfFeeds.get(position));
        this.listOfFeeds.remove(position);
        updateObservers();
    }

    public void reverseRemoval(int originalPosition) {
        Log.e(TAG, "Reversing the removal");
        //getting the last of the mohicans
        this.removedFeed = this.removedFeeds.get(this.removedFeeds.size() - 1);
        this.listOfFeeds.add(originalPosition, this.removedFeed);
        updateObservers();
    }

    public void register(NewFeed newFeed) {
        listOfObservers.add(newFeed);
        Log.e(TAG, "Observer " + newFeed + " added to the List of Observers.");
    }

    public void updateObservers() {
        for (NewFeed o : listOfObservers) {
            o.updateUI();
        }
    }

    public void addToFavorites(FeedItem feedItem) {
        this.favoriteFeedItems.add(feedItem);
        Log.e(TAG, "FeedItem added to favorite feeds list");
    }

    public void removeFromFavorites(int position) {
        this.favoriteFeedItems.remove(position);
        updateObservers();
    }

    public void generateNewFeedItems() {
        for (Feed feed : this.listOfFeeds) {
            for (int i = 0; i < 5; i++)
                this.newFeedItems.add(feed.getFeedItemList().get(i));
        }
        Log.e(TAG, "Finished adding new feeds into New Feeds list.");
    }

    public ArrayList<FeedItem> getFavoriteFeedItems() {
        return this.favoriteFeedItems;
    }

    public ArrayList<FeedItem> getNewFeedItems() {
        return this.newFeedItems;
    }

    public void replaceFeeds(ArrayList<Feed> feeds) {
        this.listOfFeeds = feeds;
        Log.e(TAG, "FEEDS LOADED");
    }
}
