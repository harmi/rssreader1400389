package matti.rssreader;

import android.util.Log;

import java.io.Serializable;

/**
 * Created by Matti on 24.9.2015.
 */
public class FeedItem {
    private static final String TAG = "RSSReader";
    private String feedItemTitle = "";
    private String feedItemLink = "";
    private String pubDate = "";
    private boolean read = false;
    private boolean favorite = false;
    transient User user = User.getUser();

    public FeedItem() {
        Log.e(TAG, "New feed item created");
    }

    public void setFeedItemTitle(String title) {
        this.feedItemTitle = title;
        Log.e(TAG, "Title added to feed " + this.feedItemTitle);
    }

    public void setFeedItemLink(String link) {
        this.feedItemLink = link;
        Log.e(TAG, "Link added to feed " + this.feedItemLink);
    }

    public void setFeedItemDate(String date) {
        this.pubDate = date;
    }

    public String getFeedItemTitle() {
        return this.feedItemTitle;
    }

    public String getFeedItemLink() {
        return this.feedItemLink;
    }

    public void printThis() {
        Log.w(TAG, "Title of this FeedItem is " + this.feedItemTitle);
        Log.w(TAG, "The link of this FeedItem is " + this.feedItemLink);
        Log.w(TAG, "The date of this FeedItem is " + this.pubDate);
    }

    public void setReadStatusTrue() {
        this.read = true;
        user.updateObservers();
    }

    public void setFavorite() {
        this.favorite = true;
        user.addToFavorites(this);
        user.updateObservers();
    }

    public void unfavorite() {
        this.favorite = false;
        user.updateObservers();
    }

    public String toString() {
        return this.feedItemTitle;
    }

    public Boolean getReadStatus() {
        return this.read;
    }

    public Boolean getFavouriteStatus() {
        return this.favorite;
    }

    public String getPubDate() {
        return this.pubDate;
    }
}
