package matti.rssreader;

import android.util.Log;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Matti on 24.9.2015.
 */
public class XMLParser {
    private static final String TAG = "RSSReader";
    private XmlPullParserFactory xmlPullParserFactory;
    private XmlPullParser xmlPullParser;
    private int evenType;
    private FeedItem feedItem;
    private Feed feed;
    private String titleName = "";
    private String link = "";
    private String pubDate = "";
    private boolean xmlParsingFinished = false;
    private User user;

    public XMLParser() throws XmlPullParserException {
        Log.e(TAG, "XML parser created");
        xmlPullParserFactory = XmlPullParserFactory.newInstance();
        Log.e(TAG, "New XML pull parser factory created.");
        xmlPullParser = xmlPullParserFactory.newPullParser();
        Log.e(TAG, "New XML pull parser created.");
        user = User.getUser();

    }

    public void parseXMLLines(InputStream xmlInputStream) throws XmlPullParserException, IOException {
        Log.e(TAG, "parseXMLLines started.");
        // setting pullparser input to the InputStream
        try {
            this.xmlPullParser.setInput(xmlInputStream, null);
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        }

        this.evenType = xmlPullParser.getEventType();
        Log.e(TAG, "Starting XML parsing loop.");

        // will loop through each tag at a time
        while (this.evenType != XmlPullParser.END_DOCUMENT) {
            // start of the document
            if (this.evenType == XmlPullParser.START_DOCUMENT) {
                Log.e(TAG, "Document starts here");

                // if the parser finds a tag start like <tag> it goes here
            } else if (this.evenType == XmlPullParser.START_TAG) {

                if (this.xmlPullParser.getName().equals("channel")) {
                    feed = new Feed();
                    Log.e(TAG, "New feed created in xmlParser");
                    this.user.addToUserFeedList(feed);
                }

                if (xmlPullParser.getName().equals("item")) {
                    Log.e(TAG, "New feed item create in xmlParser");
                    feedItem = new FeedItem();
                }
                if (feedItem != null) {
                    if (this.xmlPullParser.getName().equals("title")) {
                        this.titleName = this.xmlPullParser.nextText();
                        Log.e(TAG, "Title is: " + this.titleName);
                        feedItem.setFeedItemTitle(this.titleName);
                        // Log.w(TAG, feedItem.toString());
                        // feedItem.printThis();
                    } else if (this.xmlPullParser.getName().equals("link")) {
                        this.link = this.xmlPullParser.nextText();
                        Log.e(TAG, "Link is: " + this.link);
                        feedItem.setFeedItemLink(this.link);
                        // feedItem.printThis();
                    } else if (this.xmlPullParser.getName().equals("pubDate")) {
                        this.pubDate = this.xmlPullParser.nextText();
                        Log.e(TAG, "PubDate is: " + this.pubDate);
                        feedItem.setFeedItemDate(this.pubDate);
                        feedItem.printThis();
                    }
                } else if (this.feed != null) {
                    if (this.xmlPullParser.getName().equals("title")) {
                        this.titleName = this.xmlPullParser.nextText();
                        Log.e(TAG, "Title for the feed is: " + this.titleName);
                        feed.setFeedTitle(this.titleName);
                    } else {
                        Log.e(TAG, "Not getting a title because the feed is not made.");
                    }
                } else {
                    Log.w(TAG, "a new feedItem has not been created yet");
                }

                // if the parser finds an end tag, </tag> it goes here
            } else if (this.evenType == XmlPullParser.END_TAG) {
                //  Log.e(TAG, "End tag " + xmlPullParser.getName());

                // if parser finds and end tag, it adds the current feedItem to feed list
                if (this.xmlPullParser.getName().equals("item")) {
                    this.feed.addFeedItemsToList(feedItem);
                }

                if (this.xmlPullParser.getName().equals("channel")) {
                    //  this.user.addToUserFeedList(feed);
                }

                // inside the <tag> </tag> if it finds text, it goes here
            } else if (this.evenType == XmlPullParser.TEXT) {
                //    Log.e(TAG, "Text inside tags " + xmlPullParser.getText());
            }
            // continues to the next tag
            this.evenType = xmlPullParser.next();
        }
        Log.w(TAG, "Everything read.");
        this.feed.printFeedItemList();
        Log.w(TAG, "Trying to print out the user class.");
        Log.w(TAG, this.user.toString());
        // once the parsing is finished, updates the AllFeedItems list on user.
        // then updates observers
        this.xmlParsingFinished = true;
        user.updatedAllFeedItemsList();
        user.updateObservers();
    }

    // returns boolean to check if going through the feed is done
    public boolean isXmlParsingFinished() {
        return this.xmlParsingFinished;
    }


}

//setonitemclicklistener