package matti.rssreader;

import android.content.Intent;
import android.net.Uri;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;

public class FavoriteActivity extends AppCompatActivity implements NewFeed, NavigationView.OnNavigationItemSelectedListener {
    public static final String TAG = "RSSReader";
    private User user;
    private CustomDrawerLayout drawerLayout;
    private NavigationView navigationView;
    private Toolbar toolbar;
    private ListView listView;
    private LinearLayout main_content_favoritesView;
    private FavoriteFeedsAdapter favoriteFeedsAdapter;
    private Intent browserIntent;
    private FeedItem feedItem;
    private Intent feedViewIntent;
    private Intent favoritesIntent;
    private Intent settingsIntent;
    private Intent newFeedItemsIntent;
    private String[] menuItemsUnlike = {"Unfavorite", "Share"};
    private int listItemClicked;
    private int menuItemClicked;
    private Intent shareIntent;
    private CoordinatorLayout coordinatorLayoutView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorite);
        user = User.getUser();
        setupNavigationView();
        setupToolbar();
        this.listView = (ListView) findViewById(R.id.listView);
        this.coordinatorLayoutView = (CoordinatorLayout) findViewById(R.id.main_content_feedview);
        this.main_content_favoritesView = (LinearLayout) findViewById(R.id.main_content_favoritesView);
        registerToFeed();
        // registering the listview in order to enable pop up menu
        registerForContextMenu(this.listView);
        makeListView();
        // makes feedItems clickable -> opens it in the browser
        this.listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                feedItem = favoriteFeedsAdapter.getItem(position);
                // setting the feeditem as read
                favoriteFeedsAdapter.getItem(position).setReadStatusTrue();
                browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(feedItem.getFeedItemLink()));
                startActivity(browserIntent);
            }
        });
    }

    private void makeListView() {
        this.favoriteFeedsAdapter = new FavoriteFeedsAdapter(this, R.layout.activity_favorite, user.getFavoriteFeedItems());
        this.listView.setAdapter(this.favoriteFeedsAdapter);
    }

    @Override
    public void updateUI() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                user.printAllFeedItems();
                favoriteFeedsAdapter.notifyDataSetChanged();
            }
        });
    }

    public void registerToFeed() {
        user.register(this);
    }

    // setting up the toolbar
    private void setupToolbar() {
        this.toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(this.toolbar);
        final ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeAsUpIndicator(R.drawable.ic_menu_black_24dp);
        actionBar.setDisplayHomeAsUpEnabled(true);
    }

    // finds drawerLayout & sets this activity to NavigationItemSelectedListener (onClick)
    private void setupNavigationView() {
        this.drawerLayout = (CustomDrawerLayout) findViewById(R.id.drawerLayout);
        this.navigationView = (NavigationView) findViewById(R.id.navigationView);
        this.navigationView.setNavigationItemSelectedListener(this);
    }

    //  opens the NavigationView inside the DrawerLayout
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.e(TAG, "ID clicked " + item.getItemId());
        switch (item.getItemId()) {
            // user clicks the home button -> opens the menu
            case android.R.id.home:
                Log.e(TAG, "Home button clicked");
                this.drawerLayout.openDrawer(GravityCompat.START);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    // listener for the NavigationView items
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        Log.e(TAG, "ID clicked " + item.getItemId());
        switch (item.getItemId()) {
            case R.id.list_of_feeds:
                Log.e(TAG, "Accessing feedview");
                this.feedViewIntent = new Intent(this, FeedView.class);
                startActivity(this.feedViewIntent);
                this.drawerLayout.closeDrawer(GravityCompat.START);
                return true;
            case R.id.favorites:
                Log.e(TAG, "Accessing favorites");
                this.favoritesIntent = new Intent(this, FavoriteActivity.class);
                startActivity(this.favoritesIntent);
                this.drawerLayout.closeDrawer(GravityCompat.START);
                return true;
            case R.id.new_feed_items:
                Log.e(TAG, "Accessing NEW");
                this.newFeedItemsIntent = new Intent(this, NewFeedItemsActivity.class);
                startActivity(this.newFeedItemsIntent);
                this.drawerLayout.closeDrawer(GravityCompat.START);
                return true;
       /*     case R.id.settings:
                Log.e(TAG, "Accessing settings");
                //this.settingsIntent = new Intent(this, settingsActivity.class);
                //startActivity(this.settingsActivity);
                this.drawerLayout.closeDrawer(GravityCompat.START);
                return true; */
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_favorite, menu);
        return true;
    }

    // creates a pop up menu when long pressing a ListItem
    @Override
    public void onCreateContextMenu(ContextMenu menu, View view, ContextMenu.ContextMenuInfo menuInfo) {
        if (view.getId() == R.id.listView) {
            AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
            Log.e(TAG, "In favorite activity user long pressed item in pos " + info.position);
            this.listItemClicked = info.position;
            menu.setHeaderTitle("Feed item");
            for (int i = 0; i < this.menuItemsUnlike.length; i++) {
                menu.add(menu.NONE, i, i, this.menuItemsUnlike[i]);
            }
        }
    }

    // get info on what pop up menu item user clicked
    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        this.menuItemClicked = item.getItemId();
        // unfavorite item
        if (this.menuItemClicked == 0) {
            Log.e(TAG, "User chose to unfavorite the article");
            user.removeFromFavorites(this.listItemClicked);
            makeSnackBar();
            // share item
        } else if (this.menuItemClicked == 1) {
            shareLink();
        }
        return true;
    }

    public void makeSnackBar() {
        Snackbar.make(this.coordinatorLayoutView, "Article unfavorited.", Snackbar.LENGTH_SHORT)
                .show();
    }

    // sharing a link
    public void shareLink() {
        this.shareIntent = new Intent(android.content.Intent.ACTION_SEND);
        this.shareIntent.setType("text/plain");
        this.shareIntent.putExtra(Intent.EXTRA_SUBJECT, user.getFavoriteFeedItems().get(this.listItemClicked).getFeedItemTitle());
        this.shareIntent.putExtra(Intent.EXTRA_TEXT, "Check out this link " + user.getFavoriteFeedItems().get(this.listItemClicked).getFeedItemLink());
        startActivity(Intent.createChooser(this.shareIntent, "SHARE THIS LINK"));
    }
}
